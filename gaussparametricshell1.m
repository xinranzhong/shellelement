%__________________________________________________________________________
%| 4NODE-20DOF CURVED PARAMETRIC SHELL FEM                  [A.] (C)(R)   |
%|########################################                                 |
%|                               Shape function:Serendipty Family          |
%|                               Stifness      :Topology+Accumulate method |
%|_______________________________Solve Equation:Cholesky-[L][D][u]_________|
%|                                                                         |
%| Maxima 5.9.0.9beta2 http://maxima.sourceforge.net                       |
%| Using Lisp Kyoto Common Lisp GCL 2.6.3 (aka GCL)                        |
%| Distributed under the GNU Public License. See the file COPYING.         |
%| Dedicated to the memory of William Schelter.                            |
%| This is a development version of Maxima. The function bug_report()      |
%| provides bug reporting information.                                     |
%|_________________________________________________________________________|

clc
clear 
                                     %##################INPUT DATA###############
                                     
%####################### MATERIALS PROPERTIES
E=2E6;        %shell material elasticity constant;
v=0.3;        %shell material poission ratio;
%#######################

%####################### AUTOMATIC MESH DATA
th=0.10;   %local "z" axis direction thickness
totala       = 2.00;        %"x" axis direction total length
totalb       = 2.00;        %"y" axis direction total width
meshx   = 2;           %"x" axis direction mesh number
meshy   = 2;           %"y" axis direction mesh number
segment = 2;           %"z" axis direction segment value
%it`s stress-strain segment surface(examp;composite materials)
%#######################
gridspacex = totala/meshx;  % along to "x" axis direction mesh space
gridspacey = totalb/meshy;  % along to "y" axis direction mesh space

%##################### MESH SURFACE FUNCTIONS
%fshell=inline('-1/10*(x)^2-1/10*(y)^2+5');
%#####################


%Mesh error control
if meshx <1 || meshy <1 || segment <1;
    display('Open dimension system mesh and this mesh value > 1')
%    [meshx meshy]
    error('mesh values')
else if meshx >10 || meshy>10 || segment >10;   
    display('system analysis mesh is big')
    end
end



%_____________________________________GLOBAL COORDINATES
dim=1;
for sut=0:meshy 
     for sat=0:meshx %Element middle node reference 
x=sat*gridspacex-meshx*gridspacex/2;
y=sut*gridspacey-meshy*gridspacey/2;
     Cor(dim,1)=x;   %"x" global coordinate value 
     Cor(dim,2)=y;   %"y" global coordinate value 
     Cor(dim,3)=-1/10*(x)^2-1/10*(y)^2+5;%"z" global coordinates value
    %Cor(dim,3)=fshell(x,y);%"z" global coordinates value
     dim=dim+1;    
     end
end%___________________________________________|
Number=size(Cor);clear x y;
Node=Number(1);                     %Elements Number


%____________________________________POSITION MATRIX        
no=1;       %Pos(Shell Element No,:)=[Shell Element Node Number]        
for i=1:meshy+1;
    for j=1:meshx;
        if  i < meshy+1 && j<meshx+1;
            Pos(no,1)=(meshx+1)*(i-1)+j;
            Pos(no,2)=(meshx+1)*(i-1)+j+1;
            Pos(no,3)=(meshx+1)*(i)+j+1;
            Pos(no,4)=(meshx+1)*(i)+j;
        end
        no=no+1;    
    end
end%________________________________________|
Number=size(Pos);
No=Number(1);   %Plane Elements Number


%Shell`s Node Topology
for i=1:Node;
    Re(i,:)=[1 1 1 1 1];
end
%Re(Node number,:)=[ux vy wz theta  beta]
%############################SYSTEM SUPPORTS
 Re(1,:)=[0 0 0 0 0];
 Re(3,:)=[0 0 0 0 0];
 Re(7,:)=[0 0 0 0 0];
 Re(9,:)=[0 0 0 0 0];
 %#############################
Topology=Re;

%_________________________________ACCUMULATE
Number=size(Re);
Nom=Number(2);                   %Element node d.o.f Number
clear Number
sayman=0;
for i=1:Node;
    for j=1:Nom;
        if Re(i,j)==1 ;
            sayman = sayman +1;
            Re(i,j) = sayman;
        end
    end
end%_________________________________|
Item=sayman;

%_________________________________MODAL DISPLACEMENTS
for i=1:No
R(i,:)=[Re(Pos(i,1),:) Re(Pos(i,2),:) Re(Pos(i,3),:) Re(Pos(i,4),:)];
end
%for i=1:No;
%    for j=1:Nom;
%        R(i,j+Nom*0) = Re(Pos(i,1),j);
%        R(i,j+Nom*1) = Re(Pos(i,2),j);
%        R(i,j+Nom*2) = Re(Pos(i,3),j);
%        R(i,j+Nom*3) = Re(Pos(i,4),j);
%    end
%end%_________________________________________|

%######################SYSTEM LOADS
%P(Re(Node number, Freedoom number))=[u v w theta beta]
 P(Item)=0;
 P(Re(2,3))=-5;
 P(Re(4,3))=-5;
 P(Re(6,3))=-5;
 P(Re(8,3))=-5;
%#######################

%___________________________________________NODE COORDINATES
%Moving shell global coordinates element node`s                              
for Element=1:No;
    px(Element,:)=[Cor(Pos(Element,1),1)...
                   Cor(Pos(Element,2),1)...
                   Cor(Pos(Element,3),1)...
                   Cor(Pos(Element,4),1)];
               
    py(Element,:)=[Cor(Pos(Element,1),2)...
                   Cor(Pos(Element,2),2)...
                   Cor(Pos(Element,3),2)...
                   Cor(Pos(Element,4),2)];
    
    pz(Element,:)=[Cor(Pos(Element,1),3)...
                   Cor(Pos(Element,2),3)...
                   Cor(Pos(Element,3),3)...
                   Cor(Pos(Element,4),3)];
end%____________________________|


%________________________________________SHELL SPECIAL TRANFORMATION 
for Element=1:No;
pxo5=(px(Element,1)+px(Element,2))/2;
pyo5=(py(Element,1)+py(Element,2))/2;
pzo5=(pz(Element,1)+pz(Element,2))/2;

pxo6=(px(Element,2)+px(Element,3))/2;
pyo6=(py(Element,2)+py(Element,3))/2;
pzo6=(pz(Element,2)+pz(Element,3))/2;

pxo7=(px(Element,3)+px(Element,4))/2;
pyo7=(py(Element,3)+py(Element,4))/2;
pzo7=(pz(Element,3)+pz(Element,4))/2;

pxo8=(px(Element,1)+px(Element,4))/2;
pyo8=(py(Element,1)+py(Element,4))/2;
pzo8=(pz(Element,1)+pz(Element,4))/2;
    
Lu1=sqrt((pxo6-pxo8)^2+(pyo6-pyo8)^2+(pzo6-pzo8)^2);
directvectorcos1(Element,:)=[(pxo6-pxo8)/Lu1 ...
                             (pyo6-pyo8)/Lu1 ...
                             (pzo6-pzo8)/Lu1];


Lu2=sqrt((pxo7-pxo5)^2+(pyo7-pyo5)^2+(pzo7-pzo5)^2);
directvectorcos2(Element,:)=[(pxo7-pxo5)/Lu2 ...
                             (pyo7-pyo5)/Lu2 ...
                             (pzo7-pzo5)/Lu2];
                            
%__________Shell middle surface thickness direction vector [V3](e3)
%__________Vectorel Multiply "x" direction and "y"  position vector{e3}=e1*e2
                          %__[l1(i)+m1(j)+n1(k)]*[l2(i)+m2(j)+n2(k)]=
                          %__[l1*l2(i*i)+l1*m2(i*j)+l1*n2(i*k)]+
                          %__[m1*l2(j*i)+m1*m2(j*j)+m1*n2(j*k)]+
                          %__[n1*l2(k*i)+n1*m2(k*j)+n1*n2(k*k)]
                          %__[l1*m2(k)+l1*n2(-j)+m1*l2(-k)+m1*n2(i)+n1*l2(j)+n1*m2(-i)
                          %__i[m1*n2-n1*m2]+j[n1*l2-l1*n2]+k[l1*m2-m1*l2]
                          
  l1=directvectorcos1(Element,1);  l2=directvectorcos2(Element,1);                          
  m1=directvectorcos1(Element,2);  m2=directvectorcos2(Element,2);                          
  n1=directvectorcos1(Element,3);  n2=directvectorcos2(Element,3);                                                    
  
                    Lu3=sqrt((m1*n2 - m2*n1)^2+ ...
                             (n1*l2 - n2*l1)^2+ ...
                             (l1*m2 - m1*l2)^2);

directvectorcos3(Element,:)=[(m1*n2-n1*m2)/Lu3 ...
                             (n1*l2-l1*n2)/Lu3 ...
                             (l1*m2-m1*l2)/Lu3];

end%________________________|
clear Lu1 Lu2 Lu3 pzo5 pzo6 pzo7 pzo8 pyo5 pyo6 pyo7 pyo8
clear pxo5 pxo6 pxo7 pxo8


%________________________________________________________DIRECTION COSINES
%This [lx,ly,lz] direction cosines for shell special axis
for Element=1:No;
pxn=[px(Element,1) px(Element,2) px(Element,3) px(Element,4)];
pyn=[py(Element,1) py(Element,2) py(Element,3) py(Element,4)];
pzn=[pz(Element,1) pz(Element,2) pz(Element,3) pz(Element,4)];

for j=1:4

lx=directvectorcos1(Element,1);  ly=directvectorcos2(Element,1);
mx=directvectorcos1(Element,2);  my=directvectorcos2(Element,2);

        %Per unit shell`s elements node rotation [Jacobian Trans.]
        %Notice this rotation angles is values get radii
%Shell surface function
%                        f(x,y)g = -1/10*(x)^2-1/10*(y)^2+5
%                       [df/dx]g = -2/10*(x)^1*1
%                       [df/dy]g = -2/10*(y)^1*1
%Moving shell global nodes cartesian node`s slopes , element special axis direction
%
% shellspecialaxis(xg) = lx*xs + ly*ys + lz*zs    [l,m,n] special-->global axis dir. cos.
% shellspecialaxis(yg) = mx*xs + my*ys + mz*zs
% shellspecialaxis(zg) = nx*xs + ny*ys + nz*zs
%                     
% [df/dxs]   [dxg/dxs    dyg/dxs]  [df/dxg]
% [      ] = [                  ]* [      ]
% [df/dys]   [dxg/dys    dyg/dys]  [df/dyg]
%            __Jacobian transform__
%
% [df/dxs]   [   lx         mx  ]  [-2/10*(x)^1*1]
% [      ] = [                  ]* [             ]
% [df/dys]   [   ly         my  ]  [-2/10*(y)^1*1]


%################## SHELL PARTIAL DERIVATIVES
tanalfax=(lx*-2*pxn(j)+mx*-2*pyn(j))/10;
tanalfay=(ly*-2*pxn(j)+my*-2*pyn(j))/10;
%################## 


alfax=atan(tanalfax);
alfay=atan(tanalfay);

 Ctr2=[       0       cos(alfax)     0;
        -cos(alfay)            0     0;
        -sin(alfay)   sin(alfax)     1];

    
directcurvecosa(:,j,Element)=[Ctr2(1,1) ; 
                              Ctr2(2,1) ;
                              Ctr2(3,1)];

directcurvecosb(:,j,Element)=[Ctr2(1,2) ; 
                              Ctr2(2,2) ;
                              Ctr2(3,2)];
    
end
end%______________________________________________________________|
clear alfax alfay tanalfax tanalfay Ctr2 lx ly mx my Elemanno j
clear px1 px2 px3 px4 py1 py2 py3 py4 pz1 pz2 pz3 pz4 px py pz



%________________________________________TRANSFORMATION MATRIX
zero  =[0 0 0 0 0;
        0 0 0 0 0;
        0 0 0 0 0;
        0 0 0 0 0;
        0 0 0 0 0];
for Element=1:No;
        l1=directvectorcos1(Element,1);
        m1=directvectorcos1(Element,2);
        n1=directvectorcos1(Element,3);                          

        l2=directvectorcos2(Element,1);                          
        m2=directvectorcos2(Element,2);                          
        n2=directvectorcos2(Element,3);                          

        l3=directvectorcos3(Element,1);                          
        m3=directvectorcos3(Element,2);                          
        n3=directvectorcos3(Element,3);                          
                
    %Node reactions transformation matrix  
                 Tc=[ l1    l2    l3     0    0;
                      m1    m2    m3     0    0;
                      n1    n2    n3     0    0;
                       0     0    0     l1   l2;
                       0     0    0     m1   m2];
%neglect--->           0     0    0     n1   n2];
 %       Information:This region has element global axis twisting components
 %neglected. Because this shell system is node slope very small and twisting
 %effect  has  depend  this node slopes. Some system model has system global
 %stiffness  matrix  is  not  invers than it`s angles very small. Generally
 %references  was  neglected  this  global  twisting effect components. For
 %example flat shell element models. Also stress-strain transform has this
 %components not neglect.

%Node stress-strain transformation matrix
Tnode(:,:,Element)=[ l1   l2   l3   0    0;
                     m1   m2   m3   0    0;
                     n1   n2   n3   0    0;
                      0   0    0   l1   l2;
                      0   0    0   m1   m2;
                      0   0    0   n1   n2];

Tson(:,:,Element)=[Tc   ,zero,zero,zero ;
                   zero,Tc   ,zero,zero ;
                   zero,zero ,Tc   ,zero;
                   zero,zero ,zero ,Tc ];
end%_______________________________________________________|
clear Tc Element l1 m1 n1 l2 m2 n2 l3 m3 n3 zero


%Open dimension element local axis stiffness matrix
K(20,20,No)=0;
%_____________________________GAUSS LEGENDRE CONSTANTS
%format long;
H=[0.171324492379170,0.360761573048139,0.467913934572691,0.467913934572691,0.360761573048139,0.171324492379170] ;
W=[0.932469514203152;
   0.661209386466265;
   0.238619186083197;
  -0.238619186083197;
  -0.661209386466265;
  -0.932469514203152];
%_________________________________________________|


%format short;
%_____________________________ELASTICITY MATRIX [C]
%Assume this element`s uniform and isotropic materials
C=E/(1-v^2)*[1  v   0     0       0     0      ; 
             v  1   0     0       0     0      ;
             0  0   0     0       0     0      ;
             0  0   0  0.5*(1-v)  0     0      ;
             0  0   0     0  5/12*(1-v) 0      ;
             0  0   0     0       0  5/12*(1-v)];
%_________________________________________________|



%_____________________________GAUSS_LEGENDRE_NUMERICAL_INTEGRATION
for s=1:No;
   for k=1:6;
     for j=1:6;
        for i=1:6;
        %[i j k s] Numerical integration numerator
        %Parametric parameters is connection homogen parameters
        a=gridspacex;
        b=gridspacey;
        x=1/2*a*W(i); %x=1/2*a*e___e=-1=>x=-a/2  and e=+1=>x=a/2
        y=1/2*b*W(j); %y=1/2*b*n___n=-1=>y=-b/2  and n=+1=>y=b/2
        z=1/2*th*W(k);%z=1/2*a*j___j=-1=>z=-th/2 and z=+1=>z=th/2
       
        %Element connection matrix is partial derivatives Hx       
        Hx1 =-(1-2*y/b)/a/2.0;
        Hx2 = (1-2*y/b)/a/2.0;
        Hx3 = (2*y/b+1)/a/2.0;
        Hx4 =-(2*y/b+1)/a/2.0;
        %Element connection matrix is partial derivatives Hy       
        Hy1 =-(1-2*x/a)/b/2.0;
        Hy2 =-(2*x/a+1)/b/2.0;
        Hy3 = (2*x/a+1)/b/2.0;
        Hy4 = (1-2*x/a)/b/2.0;
        %Element connection matrix is partial derivatives Hz       
        Hz1 = 0;
        Hz2 = 0;
        Hz3 = 0;
        Hz4 = 0;

        %Element homogen shape function
        N1=1/4*(1-(2*x/a))*(1-(2*y/b));
        N2=1/4*(1+(2*x/a))*(1-(2*y/b));
        N3=1/4*(1+(2*x/a))*(1+(2*y/b));
        N4=1/4*(1-(2*x/a))*(1+(2*y/b));

        %Element per node direction cosines
        la1=directcurvecosa(1,1,s); lb1=directcurvecosb(1,1,s);         
        ma1=directcurvecosa(2,1,s); mb1=directcurvecosb(2,1,s);
        na1=directcurvecosa(3,1,s); nb1=directcurvecosb(3,1,s);

        la2=directcurvecosa(1,2,s); lb2=directcurvecosb(1,2,s);         
        ma2=directcurvecosa(2,2,s); mb2=directcurvecosb(2,2,s);
        na2=directcurvecosa(3,2,s); nb2=directcurvecosb(3,2,s);

        la3=directcurvecosa(1,3,s); lb3=directcurvecosb(1,3,s);
        ma3=directcurvecosa(2,3,s); mb3=directcurvecosb(2,3,s);
        na3=directcurvecosa(3,3,s); nb3=directcurvecosb(3,3,s);

        la4=directcurvecosa(1,4,s); lb4=directcurvecosb(1,4,s);
        ma4=directcurvecosa(2,4,s); mb4=directcurvecosb(2,4,s);
        na4=directcurvecosa(3,4,s); nb4=directcurvecosb(3,4,s);

        %Jacobian matrix
        Jacobi=[gridspacex/2    0     ;
                   0      gridspacey/2];

        %Shell connection matrix [A]
        %[A]=[d].[N]
        %[A]=[Equbilibrium differantial matrix].[shell shape matrix]

        A(:,:,s)=[ Hx1  0    0    z*Hx1*la1           z*Hx1*lb1           Hx2   0    0    z*Hx2*la2             z*Hx2*lb2           Hx3  0     0    z*Hx3*la3           z*Hx3*lb3           Hx4    0      0     z*Hx4*la4           z*Hx4*lb4            ;               
                   0    Hy1  0    z*Hy1*ma1           z*Hy1*mb1           0     Hy2  0    z*Hy2*ma2             z*Hy2*mb2           0    Hy3   0    z*Hy3*ma3           z*Hy3*mb3           0      Hy4    0     z*Hy4*ma4           z*Hy4*mb4            ;           
                   0    0    Hz1  Hz1*na1             Hz1*nb1             0     0    Hz2  Hz2*na2               Hz2*nb2             0    0     Hz3  Hz3*na3             Hz3*nb3             0      0      Hz4   Hz4*na4             Hz4*nb4              ;            
                   Hy1  Hx1  0    z*Hy1*la1+z*Hx1*ma1 z*Hy1*lb1+z*Hx1*mb1 Hy2   Hx2  0    z*Hy2*la2+z*Hx2*ma2   z*Hy2*lb2+z*Hx2*mb2 Hy3  Hx3   0    z*Hy3*la3+z*Hx3*ma3 z*Hy3*lb3+z*Hx3*mb3 Hy4    Hx4    0     z*Hy4*la4+z*Hx4*ma4 z*Hy4*lb4+z*Hx4*mb4  ;
                   0    Hz1  Hy1  N1*ma1+z*Hy1*na1    N1*mb1+z*Hy1*nb1    0     Hz2  Hy2  N2*ma2+z*Hy2*na2      N2*mb2+z*Hy2*nb2    0    Hz3   Hy3  N3*ma3+z*Hy3*na3    N3*mb3+z*Hy3*nb3    0      Hz4    Hy4   N4*ma4+z*Hy4*na4    N4*mb4+z*Hy4*nb4     ;
                   Hz1  0    Hx1  N1*la1+z*Hx1*na1	  N1*lb1+z*Hx1*nb1    Hz2   0    Hx2  N2*la2+z*Hx2*na2	    N2*lb2+z*Hx2*nb2    Hz3  0 	   Hx3  N3*la3+z*Hx3*na3	N3*lb3+z*Hx3*nb3    Hz4    0	  Hx4   N4*la4+z*Hx4*na4	N4*lb4+z*Hx4*nb4    ];   
  
        %Gauss_Legendre_Numerical_Integration 
        %        _6_ _6_ _6_
        %  [I]=  \\  \\  \\  H(i)*H(j)*H(k)*function(W(i),W(j),W(k)) 
        %        //_ //_ //_
        %        i=1 j=1  k=1
        K(:,:,s)=K(:,:,s)+0.5*th*det(Jacobi)*H(i)*H(j)*H(k)*A(:,:,s)'*C*A(:,:,s);
        end
     end
  end
end%______________________________________________________________|
clear s k i j x y N1 N2 N3 N4 A la1 la2 la3 la4 lb1 lb2 lb3 lb4
clear Hx1 Hx2 Hx3 Hx4 Hy1 Hy2 Hy3 Hy4 Hz1 Hz2 Hz3 Hz4 Jacobi
clear ma1 ma2 ma3 ma4 na1 na2 na3 na4 mb1 mb2 mb3 mb4 nb1 nb2 nb3 nb4




%Shell stiffness matrix transformation partical to global axis
for Elemanno=1:No;
  Kg(:,:,Elemanno)=Tson(:,:,Elemanno)*K(:,:,Elemanno)*Tson(:,:,Elemanno)';
end
clear Elemanno


Ksis(Item,Item)=0;
%___________________________SYSTEM STIFFNESS MATRIX
for n=1:No;
        for sat=1:20;
            for sut=1:20;
               if (R(n,sat)~=0)
                  if (R(n,sut)~=0);
                    Ksis(R(n,sut),R(n,sat))=Ksis(R(n,sut),R(n,sat)) + Kg(sat,sut,n);
                  end%    
               end%
            end%
        end%
end%_______________________________________________|
%Ksis:Global system stiffness matrix
clear n sat sut Item


%Ksis:System global stiffness matrix
%______________________________SYSTEM DISPLACEMENT
equation=size(Ksis);
if equation(1)~=rank(Ksis)
    display('This system stiffness matrix is badly scaled')
%    R
    error('Control system support boundary conditions')
else
    Ku = inv(Ksis);
    D = Ku *P';
end%__________________________________________________|
%equation:total system freedom value
%D:global system displacements
clear equation Ku


%No:Systen total element number
%R:Reology matrix
%D:Global system displacement
%_________________________________MODAL DISPLACEMENT
 for  s = 1 : No;
        for m = 1 :20;
             u = R(s, m);
             if u ~=0; Hu(s,m,:) = D(u) ;
             else      Hu(s,m,:)  = 0   ;
             end%
        end%   
end%__________________________________________________|
%Hu:per element node`s displacements    
clear s m u 

%__________________________________________________STRESS AND STRAIN ANALYSIS    
for z=-th/2:th/segment:+th/2 ;%"z" axis direction position                 
    for s=1:No;                                                            
        for i=1:4;%Element total node number                                
            switch(i)
                    case {1},
                    %       x=-a/2; y=-b/2;  %Element node for (1)
                            e=-1  ; n=-1 ;   %Homogen coordinates
                    case {2},
                    %        x=a/2 ; y=-b/2;  %Element node for (2)
                            e= 1   ; n=-1 ;   %Homogen coordinates
                    case {3},                    
                    %        x=a/2;y=b/2;     %Element node for (3)
                             e= 1  ; n= 1 ;   %Homogen coordinates
                    otherwise,
                    %        x=-a/2;y=b/2;   %Element node for (4)
                             e=-1  ; n= 1 ;   %Homogen coordinates
             end

        x=gridspacex/2*e;%parametric axis convert homogen axis
        y=gridspacey/2*n;

        %Shape function
        N1=1/4*(1-(2*x/a))*(1-(2*y/b));
        N2=1/4*(1+(2*x/a))*(1-(2*y/b));
        N3=1/4*(1+(2*x/a))*(1+(2*y/b));
        N4=1/4*(1-(2*x/a))*(1+(2*y/b));

        %Element connection matrix is partial derivatives Hx     
        Hx1 =-(1-2*y/b)/a/2.0;
        Hx2 = (1-2*y/b)/a/2.0;
        Hx3 = (2*y/b+1)/a/2.0;
        Hx4 =-(2*y/b+1)/a/2.0;

        %Element connection matrix is partial derivatives Hy       
        Hy1 =-(1-2*x/a)/b/2.0;
        Hy2 =-(2*x/a+1)/b/2.0;
        Hy3 = (2*x/a+1)/b/2.0;
        Hy4 = (1-2*x/a)/b/2.0;

        %Element connection matrix is partial derivatives Hz       
        Hz1 = 0;
        Hz2 = 0;
        Hz3 = 0;
        Hz4 = 0;

        %Element per node direction cosines
        %Element per node direction cosines
la1=directcurvecosa(1,1,s); lb1=directcurvecosb(1,1,s);         
ma1=directcurvecosa(2,1,s); mb1=directcurvecosb(2,1,s);
na1=directcurvecosa(3,1,s); nb1=directcurvecosb(3,1,s);

la2=directcurvecosa(1,2,s); lb2=directcurvecosb(1,2,s);         
ma2=directcurvecosa(2,2,s); mb2=directcurvecosb(2,2,s);
na2=directcurvecosa(3,2,s); nb2=directcurvecosb(3,2,s);

la3=directcurvecosa(1,3,s); lb3=directcurvecosb(1,3,s);
ma3=directcurvecosa(2,3,s); mb3=directcurvecosb(2,3,s);
na3=directcurvecosa(3,3,s); nb3=directcurvecosb(3,3,s);

la4=directcurvecosa(1,4,s); lb4=directcurvecosb(1,4,s);
ma4=directcurvecosa(2,4,s); mb4=directcurvecosb(2,4,s);
na4=directcurvecosa(3,4,s); nb4=directcurvecosb(3,4,s);

        %Element connection matrix
        %z=0.50*th*W(k)
        A(:,:,s)=[ Hx1  0    0    z*Hx1*la1           z*Hx1*lb1           Hx2   0    0    z*Hx2*la2             z*Hx2*lb2           Hx3  0     0    z*Hx3*la3           z*Hx3*lb3           Hx4    0      0     z*Hx4*la4           z*Hx4*lb4            ;               
                   0    Hy1  0    z*Hy1*ma1           z*Hy1*mb1           0     Hy2  0    z*Hy2*ma2             z*Hy2*mb2           0    Hy3   0    z*Hy3*ma3           z*Hy3*mb3           0      Hy4    0     z*Hy4*ma4           z*Hy4*mb4            ;           
                   0    0    Hz1  Hz1*na1             Hz1*nb1             0     0    Hz2  Hz2*na2               Hz2*nb2             0    0     Hz3  Hz3*na3             Hz3*nb3             0      0      Hz4   Hz4*na4             Hz4*nb4              ;            
                   Hy1  Hx1  0    z*Hy1*la1+z*Hx1*ma1 z*Hy1*lb1+z*Hx1*mb1 Hy2   Hx2  0    z*Hy2*la2+z*Hx2*ma2   z*Hy2*lb2+z*Hx2*mb2 Hy3  Hx3   0    z*Hy3*la3+z*Hx3*ma3 z*Hy3*lb3+z*Hx3*mb3 Hy4    Hx4    0     z*Hy4*la4+z*Hx4*ma4 z*Hy4*lb4+z*Hx4*mb4  ;
                   0    Hz1  Hy1  N1*ma1+z*Hy1*na1    N1*mb1+z*Hy1*nb1    0     Hz2  Hy2  N2*ma2+z*Hy2*na2      N2*mb2+z*Hy2*nb2    0    Hz3   Hy3  N3*ma3+z*Hy3*na3    N3*mb3+z*Hy3*nb3    0      Hz4    Hy4   N4*ma4+z*Hy4*na4    N4*mb4+z*Hy4*nb4     ;
                   Hz1  0    Hx1  N1*la1+z*Hx1*na1	  N1*lb1+z*Hx1*nb1    Hz2   0    Hx2  N2*la2+z*Hx2*na2	    N2*lb2+z*Hx2*nb2    Hz3  0 	   Hx3  N3*la3+z*Hx3*na3	N3*lb3+z*Hx3*nb3    Hz4    0	  Hx4   N4*la4+z*Hx4*na4	N4*lb4+z*Hx4*nb4    ];   
         
        Qeglobal(:,i,s)=A(:,:,s)*Hu(s,:)';   %Element global node`s Strains
        Qsglobal(:,i,s)=C*Qeglobal(:,i,s);   %Element global node`s Stresses

        end
    Qelocal(:,:,s)=Tnode(:,:,s)'*Qeglobal(:,:,s);
    Qslocal(:,:,s)=Tnode(:,:,s)'*Qsglobal(:,:,s);
    end

fprintf ('  ######PLATE "z" axis direction SEGMENT#####  z=[% 2.4f]m \n',z);


%____________________________GLOBAL STRAINS
display('  PER ELEMENTS NODE GLOBAL STRAINS   ===[ex ey ez xy yz xz]global');
for s=1:No;
fprintf('  Element number =  ##%.f## \n',s);
fprintf('  Node number       [1]           [2]          [3]           [4] \n');
fprintf('               [% .7f]  [% .7f]  [% .7f]  [% .7f] \n',Qeglobal(:,:,s));
fprintf(' \n');
end%______________________________|
fprintf(' \n \n \n')
%Qeglobal


%____________________________LOCAL STRAINS
display (' PER ELEMENTS NODE LOCAL STRAINS   ===[ex ey ez xy yz ]local');
for s=1:No;
fprintf('  Element number =  ##%.f## \n',s);
fprintf('  Node number       [1]           [2]          [3]           [4] \n');
fprintf('               [% .7f]  [% .7f]  [% .7f]  [% .7f] \n',Qelocal(:,:,s));
fprintf(' \n');
end%______________________________|
fprintf(' \n \n \n')
%Qelocal


%____________________________GLOBAL STRESSES
display (' PER ELEMENTS NODE GLOBAL STRESSES===[ox oy oz Txy Tyz Txz]global');
for s=1:No;
fprintf('  Element number =  ##%.f## \n',s);
fprintf('  Node number       [1]           [2]          [3]           [4] \n');
fprintf('               [% .7f]  [% .7f]  [% .7f]  [% .7f] \n',Qsglobal(:,:,s));
fprintf(' \n');
end%___________________________________|
fprintf(' \n \n \n')
%Qsglobal


%____________________________LOCAL STRESSES
display (' PER ELEMENTS NODE LOCAL STRESSES  ===[ox oy oz Txy Tyz ]local');
for s=1:No;
fprintf('  Element number =  ##%.f## \n',s);
fprintf('  Node number       [1]           [2]          [3]           [4] \n');
fprintf('               [% .7f]  [% .7f]  [% .7f]  [% .7f] \n',Qslocal(:,:,s));
fprintf(' \n');
end%___________________________________|
fprintf(' \n \n \n')
%Qslocal                                                            %|


end%________________________________________________________________|
clear z i s x y gridspacex gridspacey N1 N2 N3 N4  
clear Hx1 Hx2 Hx3 Hx4 Hy1 Hy2 Hy3 Hy4 Hz1 Hz2 Hz3 Hz4
clear la ma na lb mb nb la1 la2 la3 la4 lb1 lb2 lb3 lb4
clear ma1 ma2 ma3 ma4 na1 na2 na3 na4 mb1 mb2 mb3 mb4 nb1 nb2 nb3 nb4




%Kg:Global per element`s stiffness matrix
%Hu:Global per element node`s displacement
%Tson:Transformation matrix for global to local 
%______________________________________________NODE REACTIONS
for s=1 :No;
    Pg(:,s) = Kg(:,:,s)*Hu(s,:)'; %Global reactions
    Pl(:,s)=Tson(:,:,s)'*Pg(:,s); %Local reactions
end


%_______________________________LOCAL REACTIONS
display ('====PER ELEMENTS NODE LOCAL REACTIONS===[fx fy fz Mx My]local');
for s=1:size(Pl,1);
fprintf('         [% .7f]  [% .7f]  [% .7f]  [% .7f] \n',Pl(s,:));
if mod(s,5)==0; fprintf(' \n');end;
end%___________________________________|
fprintf(' \n \n \n')


%_______________________________GLOBAL REACTIONS
display ('====PER ELEMENTS NODE GLOBAL REACTIONS===[fx fy fz Mx My]global');
for s=1:size(Pg,1);
fprintf('         [% .7f]  [% .7f]  [% .7f]  [% .7f]  \n',Pg(s,:));
%fprintf('         [% .7f]%.f    [% .7f]%.f   [% .7f]%.f   [% .7f]%.f  \n',Pg(s,1),R(1,s)',Pg(s,2),R(2,s)',Pg(s,3),R(3,s)',Pg(s,4),R(4,s)');
if mod(s,5)==0; fprintf(' \n');end;
end%____________________________________|
clear s No H W a b e m v sayman dim no n ans Node Nom



%_________________________________________INFORMATION
%Qs(:,:,Element number)
%Qs(:,:,100) =
%
%  1.0e+003 *
%     [1]        [2]       [3]       [4]  Element node number--->
%    1.8487    1.4243    0.0098    0.4341 ox
%    1.8487    0.4341    0.0098    1.4243 oy
%         0         0         0         0 oz
%   -1.1700   -1.6651   -2.1602   -1.6651 Txy
%    0.0385         0   -0.0094   -0.0418 Tyz
%    0.0385   -0.0418   -0.0094         0 Txz
%                                           /|\
%                          Stress components |

%_____________________________ANALYSIS RESULTS(right hand rules)

%   [1]        [2]         [3]         [4]
%[ 0.8733]	[ 0.6199]	[-0.0000]	[ 0.2533]  fx
%[ 0.8733]	[-0.0000]	[ 0.6199]	[ 0.2533]  fy
%[ 5.0000]	[-2.5000]	[-2.5000]	[ 0.0000]  fz
%[ 1.1942]	[-0.0000]	[-1.1822]	[ 0.0245]  Mx
%[-1.1715]1	[ 1.1822]2	[ 0.0000]4	[-0.0245]5 My

%[-0.6199]	[-0.8733]	[-0.2533]	[ 0.0000]
%[-0.0000]	[ 0.8733]	[ 0.2533]	[ 0.6199]
%[-2.5000]	[ 5.0000]	[ 0.0000]	[-2.5000]
%[-0.0000]	[ 1.1942]	[ 0.0245]	[-1.1822]
%[-1.1822]2	[ 1.1715]3	[ 0.0245]5	[ 0.0000]6

%[-0.2533]	[-0.0000]	[-0.6199]	[-0.8733]
%[-0.2533]	[-0.6199]	[-0.0000]	[-0.8733]
%[ 0.0000]	[-2.5000]	[-2.5000]	[ 5.0000]
%[-0.0245]	[ 1.1822]	[-0.0000]	[-1.1942]
%[ 0.0245]5	[ 0.0000]6	[-1.1822]8	[ 1.1715]9

%[-0.0000]	[ 0.2533]	[ 0.8733]	[ 0.6199]
%[-0.6199]	[-0.2533]	[-0.8733]	[ 0.0000]
%[-2.5000]	[ 0.0000]	[ 5.0000]	[-2.5000]
%[ 1.1822]	[-0.0245]	[-1.1942]	[ 0.0000]
%[ 0.0000]4	[-0.0245]5	[-1.1715]7	[ 1.1822]8