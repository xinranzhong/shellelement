function [u,v,a]=newmark(M,C,K,F,u0,v0,dt,nt)
% newmark-beta method
%           [u,v,a]=newmark(M,C,K,F,u0,v0,dt,nt)
%           obtain the response of the dynamic system
%           M - mass matrix
%           K - stiffness matrix
%           C - damping matrix
%           F - loads matrix(nt columns)
%           u0 - initial displacement
%           v0 - initial velocity
%           dt - interval(time step)
%           nt - number of sampling points
%           [u,v,a] - disp,velocity,acceleration

    dof=length(M);
    gamma=0.5;      %gamma must >=0.5;beta must >=0.25
    beta=0.25;
    error=1e-5;     %set iteration error
    
    % Constants used in Newmark's integration
    a0=1/(beta*(dt)^2);  
    a1=gamma/(beta*dt);
    a2=1/(beta*dt);
    a3=1/(2*beta)-1;
    a4=gamma/beta-1;
    a5=0.5*dt*((gamma/beta)-2);
    a6=dt*(1-gamma);
    a7=gamma*dt;

    u=zeros(dof,nt);
    v=zeros(dof,nt);
    a=zeros(dof,nt);
    u(:,1)=u0;
    v(:,1)=v0;
   
    
    Ke=a0*M+a1*C+K;
    
    
    for i=1:nt
        ut=u(:,i);  %initial the variables for the first iteration
        vt=v(:,i);
        at=a(:,i);
        F0=F*cos(pi*i*dt);
        at=M\(F0-C*vt-K*ut);
            Rcap = F0+M*(a0*ut+a2*vt+a3*at)+C*(a1*ut+a4*vt+a5*at);
            utt=Ke\Rcap;
            att = a0*(utt-ut)-a2*vt-a3*at;
            vtt = vt+a6*at+a7*att;
            ut = utt ;
            vt = vtt ;
            at = att ;
        u(:,i+1)=utt;
        v(:,i+1)=vtt;
        a(:,i+1)=att;
    end       
end

