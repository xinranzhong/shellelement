function [u,v,a]=newmarkPOD(M,C,K,F,u0,v0,dt,nt,m)
% newmark-beta method
%           [u,v,a]=newmark(M,C,K,F,u0,v0,dt,nt)
%           obtain the response of the dynamic system
%           M - mass matrix
%           K - stiffness matrix
%           C - damping matrix
%           F - loads matrix(nt columns)
%           u0 - initial displacement
%           v0 - initial velocity
%           dt - interval(time step)
%           nt - number of sampling points
%           m - dimension of model reduction
%           [u,v,a] - disp,velocity,acceleration

    dof=length(M);
    gamma=0.5;      %gamma must >=0.5;beta must >=0.25
    beta=0.25;
    error=1e-5;     %set iteration error
    
    % Constants used in Newmark's integration
    a0=1/(beta*(dt)^2);  
    a1=gamma/(beta*dt);
    a2=1/(beta*dt);
    a3=1/(2*beta)-1;
    a4=gamma/beta-1;
    a5=0.5*dt*((gamma/beta)-2);
    a6=dt*(1-gamma);
    a7=gamma*dt;

    u=zeros(dof,nt);
    v=zeros(dof,nt);
    a=zeros(dof,nt);
    u(:,1)=u0;
    v(:,1)=v0;
   
    
    Ke=a0*M+a1*C+K;

    for i=1:3*m         %compute a certain time       
        ut=u(:,i);  %initial the variables for the first iteration
        vt=v(:,i);
        at=a(:,i);
        F0=F*cos(pi*i*dt);
        at=M\(F0-C*vt-K*ut);
            Rcap = F0+M*(a0*ut+a2*vt+a3*at)+C*(a1*ut+a4*vt+a5*at);
            utt=Ke\Rcap;
            att = a0*(utt-ut)-a2*vt-a3*at;
            vtt = vt+a6*at+a7*att;
            ut = utt ;
            vt = vtt ;
            at = att ;
        u(:,i+1)=utt;
        v(:,i+1)=vtt;
        a(:,i+1)=att;
    end  
    
    phi=zeros(dof,m);   %assemble the reduction matrix fai
    fm=[3 5 9 11 15];
    phi=[u(:,fm)];
    
    X=phi;
    % calculate the mean of the snapshots
    nsnap = size(X,2);
    Xmean = sum(X,2)/nsnap;
    % Obtain new snapshot ensemble with zero mean
    for i=1:nsnap
        X1(:,i) = X(:,i)-Xmean;
    end
    % calculate the empirical correlation matrix C
    C1 = X1'*X1/nsnap;
    % Calculate the POD basis
    [evectorC,evalueC] = eig(C1);
    phi = X1 * evectorC;
    % Normalize the POD basis
    for i=1:nsnap
        phi(:,i) = phi(:,i)/norm(phi(:,i),2);
    end
    % return the POD eigenvalues in a vector
    lam = diag(evalueC);
    % Rearrange POD eigenvalues, vectors in descending order.
    lam = rot90(lam,2);
    phi = fliplr(phi);
    
    for i=1:nt
        ut=u(:,i);  %initial the variables for the first iteration
        vt=v(:,i);
        at=a(:,i);
        F0=F*cos(pi*i*dt);
        at=M\(F0-C*vt-K*ut);
            Rcap = F0+M*(a0*ut+a2*vt+a3*at)+C*(a1*ut+a4*vt+a5*at);
            utt=Ke\Rcap;
            att = a0*(utt-ut)-a2*vt-a3*at;
            vtt = vt+a6*at+a7*att;
            ut = utt ;
            vt = vtt ;
            at = att ;
        u(:,i+1)=utt;
        v(:,i+1)=vtt;
        a(:,i+1)=att;
    end     
end

