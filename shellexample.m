clc;
clear all;
%==========================================================================
% PART I   PRE-PROCESSING
%==========================================================================

%Material Properties-------------------------------------------------------
E=2e6;           %material elasticity constant;
v=0.3;           %material poission ratio;
th=0.1;          %local "z" axis direction thickness

%Mesh Data-----------------------------------------------------------------
segment=2;       %"z" axis direction segment
totalx= 2.00;    %"x" axis direction total length
totaly= 2.00;    %"y" axis direction total width
meshx=2  ;       %"x" axis direction mesh value
meshy=2  ;       %"y" axis direction mesh value
elementlengthx = totalx/meshx;  %along to "x" axis direction mesh space
elementlengthy = totaly/meshy;  %along to "y" axis direction mesh space
fshell=inline('-1/10*(x)^2-1/10*(y)^2+5');%mesh surface function
noe=meshx*meshy;                %number of elements
non=(meshx+1)*(meshy+1);        %number of nodes
dof=5;                          %number of D.O.F for every nodes(local)

%Global Node Coordinates---------------------------------------------------
%use element middle node as reference%
coor=zeros(non,3);     %coor[ x  y  z=f(x,y)]Global Node Coordinates Matrix
k=1; 
for i=0:meshy 
     for j=0:meshx 
         x=j*elementlengthx-meshx*elementlengthx/2;
         y=i*elementlengthy-meshy*elementlengthy/2;
         coor(k,1)=x;            %"x" global coordinate value 
         coor(k,2)=y;            %"y" global coordinate value 
         coor(k,3)=fshell(x,y);  %"z" global coordinate value
         k=k+1;    
     end
end
clear k i j x y;

%Position Matrix-----------------------------------------------------------
pos=zeros(noe,4);             %pos[ 1st 2nd 3rd 4th ]Global Position Matrix
k=1;
for i=0:meshy-1
    for j=0:meshx-1
        pos(k,1)=i*meshx+j+i+1;         %1st node position of the k element
        pos(k,2)=pos(k,1)+1;            %2nd node position of the k element
        pos(k,4)=i*meshx+j+i+2+meshx;   %3rd node position of the k element
        pos(k,3)=pos(k,4)+1;            %4th node position of the k element
        k=k+1;
    end
end
clear k i j;

%Node Topology-------------------------------------------------------------
ndof=zeros(non,dof);                     %Global D.O.F Number of Every Node
k=0;
for i=1:non
    for j=1:dof
        k=k+1;
        ndof(i,j)=k;
    end
end
total=k;                                  %Total Number of D.O,F(variables)
clear k i j;
globaldofn=zeros(noe*4,dof);                      %Global D.O.F in elements
for k=1:noe
    for i=1:4
        for j=1:dof
            globaldofn(noe*(k-1)+i,:)=ndof(pos(k,i),:);
        end
    end
end
clear k i j;

%Boundary Conditions-------------------------------------------------------

%Supports (in this case, we fix 4 corners of the shell)
%u=0 v=0 w=0 thetax=0 thetay=0
ndof(1,:)=0;
ndof(meshx+1,:)=0;
ndof(non-meshx,:)=0;
ndof(non,:)=0;
%elimination the D.O.F of zero terms and renumber ndof matrix
count=1;
for i=1:non
    for j=1:dof
        if ndof(i,j)==0
            ndof(i,j)=0;
        else
            effdof=count;
            ndof(i,j)=count;
            count=count+1;
        end
    end
end
clear count i j total;
%renumber globaldofn matrix
for k=1:noe
    for i=1:4
        for j=1:dof
            globaldofn(noe*(k-1)+i,:)=ndof(pos(k,i),:);
        end
    end
end
clear k i j;

%arrange globaldofn matrix(one row one element:gdof(noe,4*dof)
gdof(noe,4*dof)=0;                    %global D.O.F number in every element
for i=1:noe*4
    r(1,:,i)=globaldofn(i,:);
end
clear i;
for i=1:noe
    gdof(i,:)=[r(:,:,(i-1)*4+1),r(:,:,(i-1)*4+2),r(:,:,(i-1)*4+3),r(:,:,i*4)];
end
clear i;

%Loads---------------------------------------------------------------------
%F(ndof(Node number, Freedoom number))=[Fx Fy Fz Mx My]
 F(effdof)=0;
 F(ndof(2,3))=-5;
 F(ndof(4,3))=-5;
 F(ndof(6,3))=-5;
 F(ndof(8,3))=-5;

%==========================================================================
% PART II   SOLVER
%==========================================================================
 
%Node Coordinates----------------------------------------------------------
%Put global nodes' coordinates to elements
%coorxg=x,cooryg=y,coorzg=z (dcoorxg,cooryg,coorzg are global coordinates)
for k=1:noe
    coorxg(k,:)=[coor(pos(k,1),1) coor(pos(k,2),1)...
               coor(pos(k,3),1) coor(pos(k,4),1)];
    cooryg(k,:)=[coor(pos(k,1),2) coor(pos(k,2),2)...
               coor(pos(k,3),2) coor(pos(k,4),2)];
    coorzg(k,:)=[coor(pos(k,1),3) coor(pos(k,2),3)...
               coor(pos(k,3),3) coor(pos(k,4),3)];
end
clear k;
%Element Transformation----------------------------------------------------
%establish the local coordination
for k=1:noe
    coorx5(k,:)=(coorxg(k,1)+coorxg(k,2))/2;      %the midpoint of 1-2 edge
    coory5(k,:)=(cooryg(k,1)+cooryg(k,2))/2;
    coorz5(k,:)=(coorzg(k,1)+coorzg(k,2))/2;
    coorx6(k,:)=(coorxg(k,2)+coorxg(k,3))/2;      %the midpoint of 2-3 edge
    coory6(k,:)=(cooryg(k,2)+cooryg(k,3))/2;
    coorz6(k,:)=(coorzg(k,2)+coorzg(k,3))/2;
    coorx7(k,:)=(coorxg(k,3)+coorxg(k,4))/2;      %the midpoint of 3-4 edge
    coory7(k,:)=(cooryg(k,3)+cooryg(k,4))/2;
    coorz7(k,:)=(coorzg(k,3)+coorzg(k,4))/2;
    coorx8(k,:)=(coorxg(k,1)+coorxg(k,4))/2;      %the midpoint of 1-4 edge
    coory8(k,:)=(cooryg(k,1)+cooryg(k,4))/2;
    coorz8(k,:)=(coorzg(k,1)+coorzg(k,4))/2;
    
    %calculate x-axis of local coordinates 
    lxl=...
    sqrt((coorx6(k)-coorx8(k))^2+(coory6(k)-coory8(k))^2+(coorz6(k)-coorz8(k))^2);
    lcosx(k,:)=[(coorx6(k)-coorx8(k))/lxl...
                (coory6(k)-coory8(k))/lxl...
                (coorz6(k)-coorz8(k))/lxl];
    %calculate y-axis of local coordinates 
    lyl=...
    sqrt((coorx7(k)-coorx5(k))^2+(coory7(k)-coory5(k))^2+(coorz7(k)-coorz5(k))^2);
    lcosy(k,:)=[(coorx7(k)-coorx5(k))/lyl...
                (coory7(k)-coory5(k))/lyl...
                (coorz7(k)-coorz5(k))/lyl];
    
    %calculate z-axis of local coordinates
    %Vector x=(x1, y1, z1),y=(x2, y2, z2) %z is the cross-product of x and y
    x1=lcosx(k,1);                           
    y1=lcosx(k,2);                           
    z1=lcosx(k,3);   
    x2=lcosy(k,1);
    y2=lcosy(k,2);
    z2=lcosy(k,3);
    lzl=sqrt((y1*z2 - y2*z1)^2+(z1*x2 - z2*x1)^2+(x1*y2 - x2*y1)^2);
    lcosz(k,:)=[(y1*z2 - y2*z1)/lzl (z1*x2 - z2*x1)/lzl (x1*y2 - x2*y1)/lzl];            
end
clear k lxl lyl lzl;
clear coorx5 coory5 coorz5 coorx6 coory6 coorz6;
clear coorx7 coory7 coorz7 coorx8 coory8 coorz8;

%Direction Cosines---------------------------------------------------------
for k=1:noe
    px=coorxg(k,:);
    py=cooryg(k,:);
    pz=coorzg(k,:);
    for j=1:4
        lx=lcosx(k,1);  ly=lcosy(k,1);
        mx=lcosx(k,2);  my=lcosy(k,2);
        %Jacobian' Transformation
        %______________________________________%
        %|fx=d(fshell)/dx  fy=d(fshell)/dy     |
        %|tanthetax1=lx*fx(px(j))+mx*fy(py(j)) |
        %|tanthetax1=ly*fx(px(j))+my*fy(py(j)) |
        %--------------------------------------%
        tanthetaxl=lx*(-1/5)*px(j)+mx*(-1/5)*py(j);
        tanthetayl=ly*(-1/5)*px(j)+my*(-1/5)*py(j);
        thetax=atan(tanthetaxl);
        thetay=atan(tanthetayl);
        
        trans=[       0       cos(thetax)    0;
                 -cos(thetay)      0         0;
                 -sin(thetay) sin(thetax)    1];

    
        cosa(:,j,k)=[trans(1,1);trans(2,1);trans(3,1)];

        cosb(:,j,k)=[trans(1,2);trans(2,2);trans(3,2)];      
    end
end
clear tanthetaxl tanthetayl thetax thetay trans;
clear px py pz k j;

%Transformation Matrix-----------------------------------------------------
null=zeros(dof,dof);
for k=1:noe
    l1=lcosx(k,1);
    m1=lcosx(k,2);
    n1=lcosx(k,3);                          

    l2=lcosy(k,1);                          
    m2=lcosy(k,2);                          
    n2=lcosy(k,3);                          

    l3=lcosz(k,1);                          
    m3=lcosz(k,2);                          
    n3=lcosz(k,3);                          
                
    %Local Transformation Matrix
    Tl=[ l1   l2   l3    0    0;                
         m1   m2   m3    0    0;
         n1   n2   n3    0    0;
         0    0    0    l1   l2;
         0    0    0    m1   m2];
    
    %Stress-Strain Transformation Matrix[Tn]-------------------------------
    Tn(:,:,k)=[ l1   l2   l3   0    0;
                m1   m2   m3   0    0;
                n1   n2   n3   0    0;
                0    0    0   l1   l2;
                0    0    0   m1   m2;
                0    0    0   n1   n2];
   
    %Global Transformation Matrix[Tg]--------------------------------------
    Tg(:,:,k)=[Tl  ,null,null,null;
               null,Tl  ,null,null;
               null,null,Tl  ,null;
               null,null,null,Tl ];
end
clear  k;
clear l1 l2 l3 m1 m2 m3 n1 n2 n3;

%Calculate Local Element Stiffness Matrix[Ke]------------------------------
Ke(4*dof,4*dof,noe)=0;                      %local element sfiffness matrix

%Elasticity Matrix[C]------------------------------------------------------
%isotropical and uniform
C=E/(1-v^2)*[1  v   0     0       0     0      ; 
             v  1   0     0       0     0      ;
             0  0   0     0       0     0      ;
             0  0   0  0.5*(1-v)  0     0      ;
             0  0   0     0  5/12*(1-v) 0      ;
             0  0   0     0       0  5/12*(1-v)];

%Gauss Integration
gp=[0.932469514203152;                                      %6 gauss points
    0.661209386466265;
    0.238619186083197;
   -0.238619186083197;
   -0.661209386466265;
   -0.932469514203152];
w=[0.171324492379170,...                             %6 weighting constants
   0.360761573048139,...
   0.467913934572691,...
   0.467913934572691,...
   0.360761573048139,...
   0.171324492379170];

for e=1:noe                                       %count the element number
    for k=1:6                                             %integration of z
        for j=1:6                                         %integration of y
            for i=1:6                                     %integration of x
                %integration function
                x=1/2*elementlengthx*gp(i);
                y=1/2*elementlengthy*gp(j);
                z=1/2*th*gp(k);
                
                %partial derivatives Hx       
                Hx1 =-(1-2*y/elementlengthy)/elementlengthx/2;
                Hx2 = (1-2*y/elementlengthy)/elementlengthx/2;
                Hx3 = (2*y/elementlengthy+1)/elementlengthx/2;
                Hx4 =-(2*y/elementlengthy+1)/elementlengthx/2;
                
                %partial derivatives Hy       
                Hy1 =-(1-2*x/elementlengthx)/elementlengthy/2;
                Hy2 =-(2*x/elementlengthx+1)/elementlengthy/2;
                Hy3 = (2*x/elementlengthx+1)/elementlengthy/2;
                Hy4 = (1-2*x/elementlengthx)/elementlengthy/2;
                
                %partial derivatives Hz       
                Hz1 = 0;
                Hz2 = 0;
                Hz3 = 0;
                Hz4 = 0;

                %shape function[N]-----------------------------------------
                N1=1/4*(1-(2*x/elementlengthx))*(1-(2*y/elementlengthy));
                N2=1/4*(1+(2*x/elementlengthx))*(1-(2*y/elementlengthy));
                N3=1/4*(1+(2*x/elementlengthx))*(1+(2*y/elementlengthy));
                N4=1/4*(1-(2*x/elementlengthx))*(1+(2*y/elementlengthy));
                
                %direction cosines
                la1=cosa(1,1,e); lb1=cosb(1,1,e);         
                ma1=cosa(2,1,e); mb1=cosb(2,1,e);
                na1=cosa(3,1,e); nb1=cosb(3,1,e);

                la2=cosa(1,2,e); lb2=cosb(1,2,e);         
                ma2=cosa(2,2,e); mb2=cosb(2,2,e);
                na2=cosa(3,2,e); nb2=cosb(3,2,e);

                la3=cosa(1,3,e); lb3=cosb(1,3,e);
                ma3=cosa(2,3,e); mb3=cosb(2,3,e);
                na3=cosa(3,3,e); nb3=cosb(3,3,e);

                la4=cosa(1,4,e); lb4=cosb(1,4,e);
                ma4=cosa(2,4,e); mb4=cosb(2,4,e);
                na4=cosa(3,4,e); nb4=cosb(3,4,e);
                
                A1(:,:,e)=[Hx1  0    0    z*Hx1*la1           z*Hx1*lb1          ;
                            0   Hy1  0    z*Hy1*ma1           z*Hy1*mb1          ;
                            0    0  Hz1   Hz1*na1             Hz1*nb1            ;
                           Hy1  Hx1  0    z*Hy1*la1+z*Hx1*ma1 z*Hy1*lb1+z*Hx1*mb1;
                            0   Hz1 Hy1   N1*ma1+z*Hy1*na1    N1*mb1+z*Hy1*nb1   ;
                           Hz1   0  Hx1   N1*la1+z*Hx1*na1	  N1*lb1+z*Hx1*nb1  ];
                       
                A2(:,:,e)=[Hx2  0    0    z*Hx2*la2           z*Hx2*lb2          ;
                            0   Hy2  0    z*Hy2*ma2           z*Hy2*mb2          ;
                            0    0   Hz2  Hz2*na2             Hz2*nb2            ;
                           Hy2  Hx2   0   z*Hy2*la2+z*Hx2*ma2 z*Hy2*lb2+z*Hx2*mb2;
                            0   Hz2  Hy2  N2*ma2+z*Hy2*na2    N2*mb2+z*Hy2*nb2   ;
                           Hz2  0    Hx2  N2*la2+z*Hx2*na2	  N2*lb2+z*Hx2*nb2  ];

                A3(:,:,e)=[Hx3  0    0    z*Hx3*la3           z*Hx3*lb3          ;
                            0   Hy3  0    z*Hy3*ma3           z*Hy3*mb3          ;
                            0    0   Hz3  Hz3*na3             Hz3*nb3            ;
                           Hy3  Hx3   0   z*Hy3*la3+z*Hx3*ma3 z*Hy3*lb3+z*Hx3*mb3;
                            0   Hz3  Hy3  N3*ma3+z*Hy3*na3    N3*mb3+z*Hy3*nb3   ;
                           Hz3  0    Hx3  N3*la3+z*Hx3*na3	  N3*lb3+z*Hx3*nb3  ];

                A4(:,:,e)=[Hx4  0    0    z*Hx4*la4           z*Hx4*lb4          ;
                            0   Hy4  0    z*Hy4*ma4           z*Hy4*mb4          ;
                            0    0   Hz4  Hz4*na4             Hz4*nb4            ;
                           Hy4  Hx4   0   z*Hy4*la4+z*Hx4*ma4 z*Hy4*lb4+z*Hx4*mb4;
                            0   Hz4  Hy4  N4*ma4+z*Hy4*na4    N4*mb4+z*Hy4*nb4   ;
                           Hz4  0    Hx4  N4*la4+z*Hx4*na4	  N4*lb4+z*Hx4*nb4  ];
                           
                %connection matrix[A]--------------------------------------
                A=[A1 A2 A3 A4];
                
                %Jacobian matrix
                ja=[elementlengthx/2            0        ;
                           0            elementlengthy/2];
                 
                %caculate element stiffness matrix
                Ke(:,:,e)=Ke(:,:,e)+0.5*th*det(ja)*w(i)*w(j)*w(k)*A(:,:,e)'*C*A(:,:,e);
            end
        end
    end
end
clear e i j k x y A A1 A2 A3 A4 ja;
clear Hx1 Hx2 Hx3 Hx4 Hy1 Hy2 Hy3 Hy4 Hz1 Hz2 Hz3 Hz4 N1 N2 N3 N4;
clear la1 la2 la3 la4 lb1 lb2 lb3 lb4;
clear ma1 ma2 ma3 ma4 mb1 mb2 mb3 mb4; 
clear na1 na2 na3 na4 nb1 nb2 nb3 nb4;

%Transformation of Coordinates(local-->global axis)------------------------
for k=1:noe;
  Keg(:,:,k)=Tg(:,:,k)*Ke(:,:,k)*Tg(:,:,k)';
end
clear k;

%Assemble System Stiffness Matrix[K]---------------------------------------
K(effdof,effdof)=0;
for k=1:noe
    for i=1:4*dof
        if gdof(k,i)~=0                            %pick the non-zero terms
            for j=1:4*dof
                if gdof(k,j)~=0                    %pick the non-zero terms
                    K(gdof(k,j),gdof(k,i))=K(gdof(k,j),gdof(k,i)) + Keg(i,j,k);
                end
            end
        end
    end
end
clear k i j;

%Calculate System Displacement[U]------------------------------------------
U=zeros(effdof,1);
Kinv=inv(K);
U=Kinv*F';

%Nodel Displacement in Everty Element--------------------------------------
for k=1:noe
    for i=1:4*dof
        j=gdof(k,i);
        if j==0
            NU(k,i,:)=0;
        else
            NU(k,i,:)=U(j);
        end
    end
end
clear k i j;

%Calculate Strain[Epsilon] and Stress[Sigma]-------------------------------
for z=-th/2:th/segment:th/2                    %"z" axis direction position
    for i=1:noe
        for j=1:4                            %node local number in elements
        switch(j)                                      %Homogen coordinates
            case {1},
                e=-1;n=-1;         %x=-elementlengthx/2,y=-elementlengthx/2
            case {2},
                e= 1;n=-1;         %x= elementlengthx/2,y=-elementlengthx/2
            case {3},                    
                e= 1;n= 1;         %x= elementlengthx/2,y= elementlengthx/2
            otherwise,
                e=-1;n= 1;         %x=-elementlengthx/2,y= elementlengthx/2
        end
        
        x=elementlengthx/2*e;%parametric axis convert homogen axis
        y=elementlengthy/2*n;
        
        %partial derivatives Hx       
        Hx1 =-(1-2*y/elementlengthy)/elementlengthx/2;
        Hx2 = (1-2*y/elementlengthy)/elementlengthx/2;
        Hx3 = (2*y/elementlengthy+1)/elementlengthx/2;
        Hx4 =-(2*y/elementlengthy+1)/elementlengthx/2;
                
        %partial derivatives Hy       
        Hy1 =-(1-2*x/elementlengthx)/elementlengthy/2;
        Hy2 =-(2*x/elementlengthx+1)/elementlengthy/2;
        Hy3 = (2*x/elementlengthx+1)/elementlengthy/2;
        Hy4 = (1-2*x/elementlengthx)/elementlengthy/2;
                
        %partial derivatives Hz       
        Hz1 = 0;
        Hz2 = 0;
        Hz3 = 0;
        Hz4 = 0;
        
        %shape function
        N1=1/4*(1-(2*x/elementlengthx))*(1-(2*y/elementlengthy));
        N2=1/4*(1+(2*x/elementlengthx))*(1-(2*y/elementlengthy));
        N3=1/4*(1+(2*x/elementlengthx))*(1+(2*y/elementlengthy));
        N4=1/4*(1-(2*x/elementlengthx))*(1+(2*y/elementlengthy));
          
        %direction cosines
        la1=cosa(1,1,i); lb1=cosb(1,1,i);         
        ma1=cosa(2,1,i); mb1=cosb(2,1,i);
        na1=cosa(3,1,i); nb1=cosb(3,1,i);

        la2=cosa(1,2,i); lb2=cosb(1,2,i);         
        ma2=cosa(2,2,i); mb2=cosb(2,2,i);
        na2=cosa(3,2,i); nb2=cosb(3,2,i);

        la3=cosa(1,3,i); lb3=cosb(1,3,i);
        ma3=cosa(2,3,i); mb3=cosb(2,3,i);
        na3=cosa(3,3,i); nb3=cosb(3,3,i);

        la4=cosa(1,4,i); lb4=cosb(1,4,i);
        ma4=cosa(2,4,i); mb4=cosb(2,4,i);
        na4=cosa(3,4,i); nb4=cosb(3,4,i);
        
        %connection matrix       
        A1(:,:,i)=[Hx1  0    0    z*Hx1*la1           z*Hx1*lb1          ;
                    0   Hy1  0    z*Hy1*ma1           z*Hy1*mb1          ;
                    0    0  Hz1   Hz1*na1             Hz1*nb1            ;
                   Hy1  Hx1  0    z*Hy1*la1+z*Hx1*ma1 z*Hy1*lb1+z*Hx1*mb1;
                    0   Hz1 Hy1   N1*ma1+z*Hy1*na1    N1*mb1+z*Hy1*nb1   ;
                   Hz1   0  Hx1   N1*la1+z*Hx1*na1	  N1*lb1+z*Hx1*nb1  ];
                       
        A2(:,:,i)=[Hx2  0    0    z*Hx2*la2           z*Hx2*lb2          ;
                    0   Hy2  0    z*Hy2*ma2           z*Hy2*mb2          ;
                    0    0   Hz2  Hz2*na2             Hz2*nb2            ;
                   Hy2  Hx2   0   z*Hy2*la2+z*Hx2*ma2 z*Hy2*lb2+z*Hx2*mb2;
                    0   Hz2  Hy2  N2*ma2+z*Hy2*na2    N2*mb2+z*Hy2*nb2   ;
                   Hz2  0    Hx2  N2*la2+z*Hx2*na2	  N2*lb2+z*Hx2*nb2  ];

        A3(:,:,i)=[Hx3  0    0    z*Hx3*la3           z*Hx3*lb3          ;
                    0   Hy3  0    z*Hy3*ma3           z*Hy3*mb3          ;
                    0    0   Hz3  Hz3*na3             Hz3*nb3            ;
                   Hy3  Hx3   0   z*Hy3*la3+z*Hx3*ma3 z*Hy3*lb3+z*Hx3*mb3;
                    0   Hz3  Hy3  N3*ma3+z*Hy3*na3    N3*mb3+z*Hy3*nb3   ;
                   Hz3  0    Hx3  N3*la3+z*Hx3*na3	  N3*lb3+z*Hx3*nb3  ];

        A4(:,:,i)=[Hx4  0    0    z*Hx4*la4           z*Hx4*lb4          ;
                    0   Hy4  0    z*Hy4*ma4           z*Hy4*mb4          ;
                    0    0   Hz4  Hz4*na4             Hz4*nb4            ;
                   Hy4  Hx4   0   z*Hy4*la4+z*Hx4*ma4 z*Hy4*lb4+z*Hx4*mb4;
                    0   Hz4  Hy4  N4*ma4+z*Hy4*na4    N4*mb4+z*Hy4*nb4   ;
                   Hz4  0    Hx4  N4*la4+z*Hx4*na4	  N4*lb4+z*Hx4*nb4  ];
                           
        A=[A1 A2 A3 A4];
        
        %global strain&stress in every element
        EpsilonG(:,j,i)=A(:,:,i)*NU(i,:)';                   %global strain
        SigmaG(:,j,i)=C*EpsilonG(:,j,i);                     %global stress
        end
        
    EpsilonL(:,:,i)=Tn(:,:,i)'*EpsilonG(:,:,i);               %local strain 
    SigmaL(:,:,i)=Tn(:,:,i)'*SigmaG(:,:,i);                   %local stress
    end
    
%==========================================================================
% PART III   POST-PROCESSING
%==========================================================================
    
    fprintf (' z=[% 2.4f]m \n',z);
    %Global Strain---------------------------------------------------------
    display('   node global strain of every element  =  [ex ey ez xy yz xz] G');
    for s=1:noe
        fprintf('  Element number:[%.f] \n',s);
        fprintf('  Node number        1             2             3             4 \n');
        fprintf('               [% .7f]  [% .7f]  [% .7f]  [% .7f] \n',EpsilonG(:,:,s));
        fprintf(' \n');
    end
    fprintf(' \n \n \n')

    %Local Strain----------------------------------------------------------
    display ('  node local strain of every element  =  [ex ey ez xy yz ] L');
    for s=1:noe
        fprintf('  Element number:[%.f] \n',s);
        fprintf('  Node number        1             2             3             4 \n');
        fprintf('               [% .7f]  [% .7f]  [% .7f]  [% .7f] \n',EpsilonL(:,:,s));
        fprintf(' \n');
    end
    fprintf(' \n \n \n')

    %Global Stress---------------------------------------------------------
    display ('  node global stress of every element  =  [ox oy oz Txy Tyz Txz] G');
    for s=1:noe
        fprintf('  Element number:[%.f] \n',s);
        fprintf('  Node number        1             2             3             4 \n');
        fprintf('               [% .7f]  [% .7f]  [% .7f]  [% .7f] \n',SigmaG(:,:,s));
        fprintf(' \n');
    end
    fprintf(' \n \n \n')

    %Local Stress----------------------------------------------------------
    display ('  node local stress of every element  =  [ox oy oz Txy Tyz ] L');
    for s=1:noe
        fprintf('  Element number:[%.f] \n',s);
        fprintf('  Node number        1             2             3             4 \n');
        fprintf('               [% .7f]  [% .7f]  [% .7f]  [% .7f] \n',SigmaL(:,:,s));
        fprintf(' \n');
    end
    
end
clear i j x y A A1 A2 A3 A4;
clear Hx1 Hx2 Hx3 Hx4 Hy1 Hy2 Hy3 Hy4 Hz1 Hz2 Hz3 Hz4 N1 N2 N3 N4;
clear la1 la2 la3 la4 lb1 lb2 lb3 lb4;
clear ma1 ma2 ma3 ma4 mb1 mb2 mb3 mb4; 
clear na1 na2 na3 na4 nb1 nb2 nb3 nb4;

%Node Reactions------------------------------------------------------------
for i=1 :noe
    Fg(:,i) = Keg(:,:,i)*NU(i,:)';                    %Global reactions[Fg]
    Fl(:,i)=Tg(:,:,i)'*Fg(:,i);                        %Local reactions[Fl]
end
clear i;

display ('  node local reactions of every element  =  [fx fy fz Mx My] L');
for i=1:size(Fl,1)
    fprintf('         [% .7f]  [% .7f]  [% .7f]  [% .7f] \n',Fl(i,:));
    if mod(i,5)==0
        fprintf(' \n');
    end
end
fprintf(' \n \n \n')
clear i;
display ('  node global reactions of every element  =  [fx fy fz Mx My] G');
for i=1:size(Fg,1)
    fprintf('         [% .7f]  [% .7f]  [% .7f]  [% .7f]  \n',Fg(i,:));
    if mod(i,5)==0; 
        fprintf(' \n');
    end
end
clear i;



